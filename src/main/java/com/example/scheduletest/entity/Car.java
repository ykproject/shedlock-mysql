package com.example.scheduletest.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Car {
    private String id;

    private  String name;
}
