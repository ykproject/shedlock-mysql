package com.example.scheduletest;

import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
//默认持有锁时间=30分钟
@EnableSchedulerLock(defaultLockAtMostFor = "PT10S")
//https://www.cnblogs.com/w-wu/p/16175574.html
public class ScheduleTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScheduleTestApplication.class, args);
    }

}
