package com.example.scheduletest.controller;

import com.example.scheduletest.entity.Car;
import com.example.scheduletest.service.CarService;
import net.javacrumbs.shedlock.core.SchedulerLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TestController {
    @Autowired
    CarService carService;
    //https://www.cnblogs.com/w-wu/p/16175574.html

    @Scheduled(cron = "0 38 17 * * ?") //表示每天0点执行
    //lockAtMostFor或者lockAtMostForString 的数值 要大于等于 lockAtLeastFor或lockAtLeastForString 否则就会报错
    @SchedulerLock(name = "test",lockAtLeastForString = "PT10S",lockAtMostForString = "PT10S")
    public void test() {
        Car car = new Car();
        car.setId(System.currentTimeMillis()/1000 +"");
        car.setName("ttt");
        carService.save(car);
        System.out.println(car.toString());
    }
}
