package com.example.scheduletest.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.scheduletest.entity.Car;

public interface CarService extends IService<Car> {
}
