package com.example.scheduletest.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.scheduletest.entity.Car;
import com.example.scheduletest.mapper.CarMapper;
import com.example.scheduletest.service.CarService;
import org.springframework.stereotype.Service;

@Service
public class CarServiceImpl extends ServiceImpl<CarMapper, Car> implements CarService {
}
