package com.example.scheduletest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.scheduletest.entity.Car;

public interface CarMapper extends BaseMapper<Car> {
}
