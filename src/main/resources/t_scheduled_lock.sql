/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 09/10/2022 16:59:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_scheduled_lock
-- ----------------------------
DROP TABLE IF EXISTS `t_scheduled_lock`;
CREATE TABLE `t_scheduled_lock`  (
  `NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '任务名称',
  `lock_until` timestamp(3) NULL DEFAULT NULL COMMENT '到期时间',
  `locked_at` timestamp(3) NULL DEFAULT NULL COMMENT '开始时间',
  `locked_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '分布式任务调度锁表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
